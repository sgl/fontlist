//
//  main.m
//  FontList
//
//  Created by sgl on 12-9-4.
//  Copyright (c) 2012年 sunix. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OPAppDelegate class]));
    }
}
