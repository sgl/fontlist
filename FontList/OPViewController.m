//
//  OPViewController.m
//  FontList
//
//  Created by sgl on 12-9-4.
//  Copyright (c) 2012年 sunix. All rights reserved.
//

#import "OPViewController.h"

@interface OPViewController ()

@end

@implementation OPViewController
@synthesize fontTable;
@synthesize fontFamily;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];

    self.fontFamily = [NSMutableArray arrayWithCapacity:[[UIFont familyNames] count]];
    
    for (id familyName in familyNames) {
        NSLog(@"family: %@", familyName);
        NSMutableDictionary *family = [NSMutableDictionary dictionaryWithCapacity:2];
        [family setObject:familyName forKey:@"name"];
        NSArray *fontNames = [NSArray arrayWithArray:[UIFont fontNamesForFamilyName:familyName]];
        NSMutableArray *fonts = [NSMutableArray arrayWithCapacity:[fontNames count]];
        for (id fontName in fontNames) {
            NSLog(@"name: %@", fontName);
            NSDictionary *font = [NSDictionary dictionaryWithObjectsAndKeys:fontName, @"name", [UIFont fontWithName:fontName size:14.0], @"font", nil];
            [fonts addObject:font];
        }
        
        [family setObject:fonts forKey:@"fonts"];
        
        [self.fontFamily addObject:family];
    }
    
    fontTable.delegate = self;
    fontTable.dataSource = self;
    
    [fontTable reloadData];
}

- (void)viewDidUnload
{
    [self setFontTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return fontFamily.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id fontDict = [fontFamily objectAtIndex:section];
    id font = [fontDict objectForKey:@"fonts"];
    return [font count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    id fontDict = [fontFamily objectAtIndex:section];
    return [fontDict objectForKey:@"name"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"FontCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
    }
    
    id fontDict = [fontFamily objectAtIndex:indexPath.section];
    id fonts = [fontDict objectForKey:@"fonts"];
    id font = [fonts objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ 这是中文字体！English.", [font objectForKey:@"name"]];
    cell.textLabel.font = [font objectForKey:@"font"];
    
    return cell;
}

@end
