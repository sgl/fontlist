//
//  OPViewController.h
//  FontList
//
//  Created by sgl on 12-9-4.
//  Copyright (c) 2012年 sunix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *fontTable;
@property (strong, nonatomic) NSMutableArray *fontFamily;

@end
