//
//  OPAppDelegate.h
//  FontList
//
//  Created by sgl on 12-9-4.
//  Copyright (c) 2012年 sunix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
